'use strict';

describe('myApp module', function() {

  beforeEach(module('myApp'));

  describe('homeCtrl controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var homeCtrl = $controller('homeCtrl');
      expect(homeCtrl).toBeDefined();
    }));

  });
  
  describe('WeatherService module', function(){

    it('should ....', inject(function($module) {
      //spec body
      var weatherService = $module('WeatherService');
      expect(weatherService).toBeDefined();
    }));
  });
});