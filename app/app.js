'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.version',
  'WeatherService',
  'ngGeolocation'
])



// Lets set up some routing here
app.config(function ($routeProvider) {
  $routeProvider

    .when('/', {
      templateUrl: 'home/home.html',
      controller: 'HomeController'
    })

    .otherwise({
      redirectTo: '/'
    });
});

// This module will help us with gps data

// Currently having issues retrieving weather api

// var geoLocation = angular.module("ngGeolocation", []).factory("$geolocation", ["$rootScope", "$window", "$q", function (a, b, c) {
//   function d() {
//     return "geolocation" in b.navigator
//   }
//   var e = {
//     getCurrentPosition: function (e) {
//       var f = c.defer();
//       return d() ? b.navigator.geolocation.getCurrentPosition(function (b) {
//         a.$apply(function () {
//           f.resolve(b)
//         })
//       }, function (b) {
//         a.$apply(function () {
//           f.reject({
//             error: b
//           })
//         })
//       }, e) : f.reject({
//         error: {
//           code: 2,
//           message: "This web browser does not support HTML5 Geolocation"
//         }
//       }), f.promise
//     },
//     watchPosition: function (c) {
//       d() ? this.watchId || (this.watchId = b.navigator.geolocation.watchPosition(function (b) {
//         a.$apply(function () {
//           e.position = b
//         })
//       }, function (b) {
//         a.$apply(function () {
//           e.position = {
//             error: b
//           }
//         })
//       }, c)) : e.position = {
//         error: {
//           code: 2,
//           message: "This web browser does not support HTML5 Geolocation"
//         }
//       }
//     },
//     clearWatch: function () {
//       this.watchId && (b.navigator.geolocation.clearWatch(this.watchId), delete this.watchId)
//     },
//     position: void 0
//   };
//   return e
// }]);

// This service will handle data retreival related to weather.  
var WeatherService = angular.module('WeatherService', [])
WeatherService.factory('WeatherDataFactory', ['$http', function ($http) {
  //var urlBase = 'https://api.openweathermap.org/data/2.5/weather?'; // This is the correct api but it seems we may need a subscription to access it
  var urlBase = 'https://samples.openweathermap.org/data/2.5/weather?';
  var fiveDayUrlBase = 'https://samples.openweathermap.org/data/2.5/forecast?'
  var apiId = '6d2525813c133014c9f2444ead27d136';
  var defaultZipCode = '60661';
  var WeatherDataFactory = {};

  WeatherDataFactory.getGpsData = function (latitude, longitude) {
    return $http.get(urlBase + '?lat=' + latitude + '&lon=' + longitude + '&appid=' + apiId);
  };

  WeatherDataFactory.getDefaultLocationData = function () {
    return $http.get(urlBase + 'zip=' + defaultZipCode + ',us&appid=' + apiId);
  };

  WeatherDataFactory.getUserSelectedLocationData = function (cityName) {
    return $http.get(urlBase + 'q=' + cityName + '&appid=' + apiId);
  }

  WeatherDataFactory.getDefaultFiveDayForecast = function () {
    return $http.get(fiveDayUrlBase + 'zip=' + defaultZipCode + '&appid=' + apiId);
  }

  WeatherDataFactory.getUserCityFiveDayForecast = function (cityName) {
    return $http.get(fiveDayUrlBase + 'q=' + cityName + ',us&&appid=' + apiId);
  }

  WeatherDataFactory.getUserCoordinatesFiveDayForecast = function (latitude, longitude) {
    return $http.get(fiveDayUrlBase + '?lat=' + latitude + '&lon=' + longitude + '&appid=' + apiId);
  }
  return WeatherDataFactory;

}]);

app.controller('HomeController', function ($scope, WeatherDataFactory, $geolocation) {
  $scope.status;
  $scope.position;
  $scope.weatherDetails;
  // getGpsLocation(); //having trouble connecting to the weather geolocation api so we'll leave this off for now. 
  getDefaultWeatherData();

  function getGpsLocation() {
    navigator.geolocation.getCurrentPosition(function (position) {
      $scope.$apply(function () {
        $scope.position = position;
      });
    });
  }

  function getDefaultWeatherData() {
    navigator.permissions.query({
      name: 'geolocation'
    }).then(function (status) {
      if (status.state === "notGranted") { //having trouble connecting to the weather geolocation api so we'll leave this off for now. 
        $geolocation.getCurrentPosition({
            timeout: 5000
          }).then(function (position) {
            $scope.position = position;
            WeatherDataFactory.getGpsData($scope.position.coords.latitude, $scope.position.coords.longitude)
              .success(function (weather) {
                $scope.cityName = weather.name;
                $scope.currTemp = weather.main.temp;
                $scope.weatherDescription = weather.weather[0].description;
                $scope.icon = 'http://openweathermap.org/img/w/' + weather.weather[0].icon + '.png';
              })
              .error(function (error) {
                $scope.status = 'Unable to load weather data: ' + error.message;
              });
            $scope.getFiveDayForecast("gps");

          })
          .catch(function (error) {
            // at this stage, something went wrong
            // likely, user reject auto geolocation, so drop
            // down to manual. Will show code below
            console.log(error);
          })
      } else {
        WeatherDataFactory.getDefaultLocationData()
          .success(function (weather) {
            $scope.cityName = weather.name;
            $scope.currTemp = weather.main.temp;
            $scope.weatherDescription = weather.weather[0].description;
            $scope.icon = 'http://openweathermap.org/img/w/' + weather.weather[0].icon + '.png';
          })
          .error(function (error) {
            $scope.status = 'Unable to load weather data: ' + error.message;
          });
        $scope.getFiveDayForecast("default");
      }
    });
  }

  $scope.getCitiesWeather = function () {
    WeatherDataFactory.getUserSelectedLocationData($scope.cityName)
      .success(function (weather) {
        $scope.cityName = weather.name;
        $scope.currTemp = weather.main.temp;
        $scope.weatherDescription = weather.weather[0].description;
        $scope.icon = 'http://openweathermap.org/img/w/' + weather.weather[0].icon + '.png';
      })
      .error(function (error) {
        $scope.status = 'Unable to load weather data: ' + error.message;
      });

    $scope.getFiveDayForecast("cities");
  };

  $scope.getFiveDayForecast = function (typeOfForecast) {
    switch (typeOfForecast) {
      case 'default':
        WeatherDataFactory.getDefaultFiveDayForecast()
          .success(function (fiveDayForecast) {
            $scope.mondaysWeather = fiveDayForecast.list[0].main.temp;
            $scope.mondaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.tuesdaysWeather = fiveDayForecast.list[9].main.temp;
            $scope.tuesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.wednesdaysWeather = fiveDayForecast.list[17].main.temp;
            $scope.wednesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.thursdaysWeather = fiveDayForecast.list[25].main.temp;
            $scope.thursdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.fridaysWeather = fiveDayForecast.list[34].main.temp;
            $scope.fridaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';
          })
          .error(function (error) {
            $scope.status = 'Unable to load 5 day forecast data: ' + error.message;
          });
        break;
      case 'cities':
        WeatherDataFactory.getUserCityFiveDayForecast($scope.cityName)
          .success(function (fiveDayForecast) {
            $scope.mondaysWeather = fiveDayForecast.list[0].main.temp;
            $scope.mondaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.tuesdaysWeather = fiveDayForecast.list[9].main.temp;
            $scope.tuesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.wednesdaysWeather = fiveDayForecast.list[17].main.temp;
            $scope.wednesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.thursdaysWeather = fiveDayForecast.list[25].main.temp;
            $scope.thursdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.fridaysWeather = fiveDayForecast.list[34].main.temp;
            $scope.fridaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';
          })
          .error(function (error) {
            $scope.status = 'Unable to load 5 day forecast data: ' + error.message;
          });
        break;
      case 'gps':
        WeatherDataFactory.getDefaultFiveDayForecast($scope.position.coords.latitude, $scope.position.coords.longitude)
          .success(function (fiveDayForecast) {
            $scope.mondaysWeather = fiveDayForecast.list[0].main.temp;
            $scope.mondaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.tuesdaysWeather = fiveDayForecast.list[9].main.temp;
            $scope.tuesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[0].weather[0].icon + '.png';

            $scope.wednesdaysWeather = fiveDayForecast.list[17].main.temp;
            $scope.wednesdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.thursdaysWeather = fiveDayForecast.list[25].main.temp;
            $scope.thursdaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';

            $scope.fridaysWeather = fiveDayForecast.list[34].main.temp;
            $scope.fridaysWeatherIcon = 'http://openweathermap.org/img/w/' + fiveDayForecast.list[34].weather[0].icon + '.png';
          })
          .error(function (error) {
            $scope.status = 'Unable to load 5 day forecast data: ' + error.message;
          });
        break;
    }
  }
});