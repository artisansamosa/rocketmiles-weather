// This service will handle data retreival related to weather.  
var WeatherService = angular.module('WeatherService', [])
WeatherService.factory('WeatherDataFactory', ['$http', function ($http) {
  // api.openweathermap.org/data/2.5/weather?q=London,uk&callback=test
  //var urlBase = 'https://api.openweathermap.org/data/2.5/weather?'; // This is the correct api but it seems we may need a subscription to access it
  var urlBase = 'https://samples.openweathermap.org/data/2.5/weather?';
  var fiveDayUrlBase = 'https://samples.openweathermap.org/data/2.5/forecast?'
  var apiId = '6d2525813c133014c9f2444ead27d136';
  var defaultZipCode = '60661';
  var WeatherDataFactory = {};

  WeatherDataFactory.getGpsData = function (latitude, longitude) {
    return $http.get(urlBase + '?lat=' + latitude + '&lon=' + longitude + '&appid=' + apiId);
  };

  WeatherDataFactory.getDefaultLocationData = function () {
    return $http.get(urlBase + 'zip=' + defaultZipCode + ',us&appid=' + apiId);
  };

  WeatherDataFactory.getUserSelectedLocationData = function (cityName) {
    return $http.get(urlBase + 'q=' + cityName + '&appid=' + apiId);
  }

  WeatherDataFactory.getFiveDayForecast = function (cityName) {
    return $http.get(fiveDayUrlBase + 'zip=94040&appid=' + apiId);
  }
  return WeatherDataFactory;

}]);